import sys

def main():
    if len(sys.argv) != 4:
        print('Usage: python order.py {from page} {to page} {empty page}')
        sys.exit(1)

    _from = int(sys.argv[1])
    to    = int(sys.argv[2])
    empty = int(sys.argv[3])

    result = f'{get_order(_from, to, empty)}'

    print(result[1:-1])

# _from: The number of a page to begin.
# to:    The number of a page to end.
# empty: The number of a page behaving as an empty page.
def get_order(_from, to, empty):
    number_of_empty = 4 - (to - _from + 1) % 4

    xs = []

    for x in range(_from, to + 1):
        xs.append(x)

    for i in range(number_of_empty):
        xs.append(empty)

    ys = list(reversed(xs))

    ret = []

    for i, (x, y) in enumerate(zip(xs, ys)):
        if i == len(xs) / 2:
            break

        if i % 2 == 0:
            ret.append(y)
            ret.append(x)
        else:  # i % 2 == 1
            ret.append(x)
            ret.append(y)

    return ret

if __name__ == '__main__':
    main()
